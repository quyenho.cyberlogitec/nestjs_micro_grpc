import {
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Req,
  Body,
  Param,
} from '@nestjs/common';

@Controller('dogs')
export class DogsController {
  @Get()
  findAll(): string {
    return 'all dogs';
  }

  @Get(':id')
  findOne(@Param() param): string {
    return `someone ${param.id}`;
  }

  @Post('eat')
  findPost(@Body() body: Body): string {
    console.log(body);

    return 'something';
  }
}
